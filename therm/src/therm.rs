use std::error::Error;
use std::net::{ToSocketAddrs, UdpSocket};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

#[derive(Default)]
pub struct SmartTherm {
    temperature: Temperature,
    finished: AtomicBool,
}

impl SmartTherm {
    pub fn run(therm: Arc<SmartTherm>, address: impl ToSocketAddrs) -> Result<(), Box<dyn Error>> {
        let socket = UdpSocket::bind(address)?;
        socket.set_read_timeout(Some(Duration::from_secs(10)))?;

        thread::spawn(move || loop {
            if therm.is_finished() {
                return;
            }
            let mut buf = [0; 4];
            if let Err(err) = socket.recv_from(&mut buf) {
                eprintln!("can't receive datagram: {err}");
            }
            let temp = f32::from_be_bytes(buf);
            therm.set_temp(temp);
        });

        Ok(())
    }

    pub fn set_temp(&self, temp: f32) {
        self.temperature.set(temp);
    }

    pub fn get_temp(&self) -> f32 {
        self.temperature.get()
    }

    pub fn is_finished(&self) -> bool {
        self.finished.load(Ordering::SeqCst)
    }

    pub fn set_finished(&self) {
        self.finished.store(true, Ordering::SeqCst);
    }
}

impl Drop for SmartTherm {
    fn drop(&mut self) {
        self.set_finished();
    }
}

#[derive(Default)]
pub struct Temperature(Mutex<f32>);

impl Temperature {
    pub fn get(&self) -> f32 {
        *self.0.lock().unwrap()
    }

    pub fn set(&self, val: f32) {
        *self.0.lock().unwrap() = val
    }
}
