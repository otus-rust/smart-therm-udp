use std::{
    net::{SocketAddr, UdpSocket},
    thread,
    time::{Duration, Instant},
};

fn main() {
    let receiver = std::env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:4321".into());

    println!("Receiver address from args: {receiver}");

    let receiver = receiver
        .parse::<SocketAddr>()
        .expect("valid socket address expected");

    let bind_addr = "127.0.0.1:4320";
    let socket =
        UdpSocket::bind(bind_addr).unwrap_or_else(|_| panic!("can't bind socket: {}", bind_addr));
    let mut temp_gen = TemperatureGenerator::default();

    println!("Starting send temperature from {bind_addr} to {receiver}");
    loop {
        let temp = temp_gen.generate();
        let bytes = temp.to_be_bytes();
        let send_result = socket.send_to(&bytes, receiver);
        if let Err(err) = send_result {
            println!("can't send temperature: {err}")
        }
        thread::sleep(Duration::from_secs(1))
    }
}

struct TemperatureGenerator {
    started: Instant,
    current: f32,
}

impl Default for TemperatureGenerator {
    fn default() -> Self {
        Self {
            started: Instant::now(),
            current: 20.0,
        }
    }
}

impl TemperatureGenerator {
    pub fn generate(&mut self) -> f32 {
        let delay = Instant::now() - self.started;
        self.current += (delay.as_secs_f32() / 2.0).sin();
        self.current
    }
}
