use std::error::Error;
use std::sync::Arc;
use std::thread;
use std::time::Duration;

use therm::therm::SmartTherm;

fn main() -> Result<(), Box<dyn Error>> {
    let address = std::env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:4321".into());

    let therm = Arc::new(SmartTherm::default());

    SmartTherm::run(therm.clone(), address)?;

    (0..100).for_each(|_| {
        thread::sleep(Duration::from_secs(1));
        let temp = therm.get_temp();
        println!("The temperature is {temp:.1}");
    });

    Ok(())
}
